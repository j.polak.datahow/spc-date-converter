import os
import struct
from datetime import datetime, timezone

head_str = "<cccciddicccci9s9sh32s130s30siicchf48sfifc187s"
logstc_str = "<iiiii44s"
head_siz = 512
log_siz = 64
fdate_pos = 32
fdate_pos_end = 36


def check(spc_path: str):
    with open(spc_path, "rb") as fin:
        content = fin.read()
    # Find Encoding
    ftflg, fversn = struct.unpack('<cc'.encode('utf8'), content[:2])
    if fversn == b'\x4b':
        # Find Header
        header_content = struct.unpack(
            head_str.encode('utf8'), content[:head_siz])
        # ftflg, fversn, fexper, fexp, fnpts, ffirst, flast, fnsub, fxtype, fytype, fztype, fpost, fdate, fres, fsource,fpeakpt, fspare, fcmnt, fcatxt, flogoff, fmods,fprocs, flevel, fsampin,ffactor,fmethod,fzinc,fwplanes,fwinc,fwtype,freserv = header_content
        _, fversn, _, _, _, _, _, _, _, _, _, _, fdate, _, _, _, _, _, _, flogoff, _, _, _, _, _, _, _, _, _, _, _ = header_content
        d = fdate
        d_year = d >> 20
        d_month = (d >> 16) % (2**4)
        d_day = (d >> 11) % (2**5)
        d_hour = (d >> 6) % (2**5)
        d_minute = d % (2**6)
        file_date = datetime(d_year, d_month, d_day, d_hour, d_minute,
                             tzinfo=timezone.utc).strftime("%Y-%m-%dT%H:%M:%S")
        return file_date
    else:
        return None


def rewrite_spc(spc_path: str):
    with open(spc_path, "rb") as fin:
        content = fin.read()
    # Find Encoding
    _, fversn = struct.unpack('<cc'.encode('utf8'), content[:2])
    if fversn == b'\x4b':
        # Find Header
        header_content = struct.unpack(
            head_str.encode('utf8'), content[:head_siz])
        # ftflg, fversn, fexper, fexp, fnpts, ffirst, flast, fnsub, fxtype, fytype, fztype, fpost, fdate, fres, fsource,fpeakpt, fspare, fcmnt, fcatxt, flogoff, fmods,fprocs, flevel, fsampin,ffactor,fmethod,fzinc,fwplanes,fwinc,fwtype,freserv = header_content
        _, fversn, _, _, _, _, _, _, _, _, _, _, fdate, _, _, _, _, _, _, flogoff, _, _, _, _, _, _, _, _, _, _, _ = header_content
        # Find Log Data
        log_head_end = flogoff + log_siz
        logsizd, _, logtxto, _, _, _ = struct.unpack(
            logstc_str.encode('utf8'), content[flogoff:log_head_end])
        log_pos = flogoff + logtxto
        log_end_pos = log_pos + logsizd
        log_content = content[log_pos:log_end_pos].replace(
            b'\r', b'').split(b'\n')
        log_dict = dict()
        for x in log_content:
            if x.find(b'=') >= 0:
                key, value = x.split(b'=')[:2]
                log_dict[key] = value

        # Obtain correct timestamp and convert to binary representation
        fdatetime = datetime.strptime(
            log_dict[b'Acquisition_Date_Time'].decode(), "%m/%d/%Y %H:%M:%S")
        new_fdate = int(format(fdatetime.year, 'b')+format(fdatetime.month, '04b')+format(
            fdatetime.day, '05b')+format(fdatetime.hour, '05b')+format(fdatetime.minute, '06b'), 2)
        file_date = fdatetime.strftime("%Y-%m-%dT%H:%M:%S")
        new_binary_fdate = struct.pack('<i', new_fdate)

        # Replace timestamp and write new file
        new_content = content[:fdate_pos] + \
            new_binary_fdate+content[fdate_pos_end:]
        if spc_path.replace("input", "output"):
            # File gets saved in folder call output
            input_dir = os.path.dirname(spc_path)
            filename = os.path.basename(spc_path)
            output_dir = input_dir.replace("input", "output")
            spc_path = os.path.join(output_dir, filename)
            if not os.path.exists(output_dir):
                os.makedirs(output_dir)
            try:
                with open(spc_path.replace(".spc", "-converted.spc"), 'wb') as f:
                    f.write(new_content)
            except Exception as e:
                print(f"error in creating spc file of {e}")
        else:
            print("error in saving spc file in output directory")
        return file_date
    else:
        print(
            f"File type %s not supported yet. Please report issue. {hex(ord(fversn))}")
        return None


if __name__ == "__main__":
    while True:
        script_dir = os.path.dirname(os.path.realpath(__file__))
        input_dir = os.path.join(script_dir, "input")
        allnames = os.listdir(input_dir)
        filenames = [name for name in allnames if os.path.isfile(
            os.path.join(input_dir, name)) and name.endswith('.spc')]
        filenames.sort()
        print(f"Current directory {input_dir}")
        print(f"Found {len(filenames)} spc files located in current directory")
        user_input = input("Convert timestamps? (y/n/check): ")
        user_input = user_input.lower()

        if user_input == "n":
            print(f"You entered {user_input}, Process cancelled")
            break
        elif user_input == "check":
            for file in filenames:
                file_date = check(os.path.join(input_dir, file))
                print(f'File "{file}" has date "{file_date}"')
            break
        elif user_input == "y":
            print(f"Proceeding...")
            for file in filenames:
                file_date = rewrite_spc(os.path.join(input_dir, file))
                print(f'File "{file}" has been updated for date "{file_date}"')
            break
        else:
            print("Invalid input. Please enter y or n.")
            break
