# spc-date-converter

## Description
Using this script it is possible to convert date/time on spc files where metadata does not match acquisition time.

When downloading spc files, it might happen that the acquisition time of the file is overwritten with the download time. This script is prepared to read any scp file and overwrite in the corresponding bits read by any spc parser.

In short, the script will read all spc files in a folder called "input", and check if metadata file is correct. The script searches for the correct timestamp, stored on another location of the file, and overwrites the wrong data. The file is stored in a folder called "output" and renamed with "-converted" at the end of filename.


```
. (root)
 * spc-file-converter.py
 * README.md
 * input
   * example_1_spc_file_20230101-120000.spc
   * example_2_spc_file_20230102-120000.spc
 * output
   * example_1_spc_file_20230101-120000-converted.spc
   * example_2_spc_file_20230102-120000-converted.spc
```

## How to use it
1. Store all your spc files in a folder called "input" (it does not matter if the timestamp is wrong for all of them or just for some).
2. Copy python script `spc-date-converter.py` in the root folder.
3. Open your terminal/cmd and go to location of the folder.
4. Use python to run the script, `python spc-date-converter.py`.
5. Follow the instructions prompt in the terminal/cmd.
6. Corrected spectra files are stored in folder called "output" with "-converted.spc" suffix.



## Script Modes
After executing the script you will be asked if you want to "Correct Timestamps?". If:

* "Y" - Script will copy spc files to output folder with corrected date time, such that its matching acquisition time.
* "N" - Stop script execution
* "Check" - Run validation on all spc files on the folder to verify if timestamp read by parser matches expected acquisition time or corresponds to download timestamp.

